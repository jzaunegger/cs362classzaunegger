import glob
import os
import linecache
# This program reads in multiple text files
# and calculates the total time for each file. 
#
# This will only work if the files are in the
# listed directory.  
# Written By: Jackson Zaunegger

#Set path to find the txt files
fileList = glob.glob(os.path.join(os.getcwd(), "/home/zaunegger/cs362classzaunegger/assignments/Assignment5", "*.txt"))

#Create a master list of files
masterList = []

#Find all txt files and add them to the master list
for file_path in fileList:
	with open(file_path) as f_input:
		masterList.append(f_input.read())

#Open the output file
o = open("myoutput.out", "w")

#Find the names 
names = []
for file in fileList:
	currentFile  = open(file)
	nameFromFile = currentFile.readlines()
	names.append(nameFromFile[0])

#Find the number of hours in the file
for i in range(0, len(masterList[:])):
#	print(line[0])
	hours = 0
	for char in range(0, len(masterList[i])):
		if masterList[i][char] == ('h'):
			if masterList[i][char+1] == ('r'):
				hours += int(masterList[i][char -1])

#Find the number of minutes in the file	
	minutes=0
	for char in range(0, len(masterList[i])):
		if masterList[i][char] == ('m'):
			if masterList[i][char+1] == ('i'):
				if masterList[i][char+2] == ('n'):
					minutes += int(masterList[i][char-1])
					minutes += int(masterList[i][char-2])*10

# Find Totals and write to the output file
	minuteHours = minutes / 60
	actualMinutes = minutes % 60
	totalHours = minuteHours + hours
	total = str(totalHours) + ' hours ' + str(actualMinutes) + ' minutes\n'
	o.write(str(names[i]))
	o.write(total)
	o.write("\n")

