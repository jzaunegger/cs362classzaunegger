/* This is a bunch of stuff to use in pixel mapping. */ 
#include <math.h> // Allows use of pow(x,y)  ---> x^y

int InCircle(int totalrows, int totalcols, double radius, int pixRow, int pixCol){
    int InOrOut = 0; 		// Integer flag, 1 == yes, 2 == no
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;            //Distance from center to pixel.

    dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist < radius){
        InOrOut = 1;
    }
    return InOrOut;
}

int InStem(int totalrows, int totalcols, double radius, int pixRow, int pixCol){
   int InOrOut = 0;		//Flag 2 == yes, 1 == no
   int centerx = totalrows/2;
   int centery = (totalcols/2)-radius;

   int xoffset = totalrows*0.05;
   int yoffset = totalcols*0.05;

   if((centery-yoffset) < pixRow){
      if(pixRow < (centery+yoffset)){
         if((centerx-xoffset) < pixCol){  
            if(pixCol < (centerx+xoffset)){	
	       InOrOut = 2;
            }
         }
      }
   }
   return InOrOut;
}

int mouth(int totalrows, int totalcols, double radius, int pixRow, int pixCol){
   int InOrOut = 0;             //Flag 3 ==yes, 0 ==no     
   int centerx = totalrows/2;
   int centery = totalcols/2;
   int dist = 0;

   radius = radius*0.2;

   dist = pow((pow(centerx - pixCol,2))+(pow(centery - pixRow,2)),0.5);

   if (dist < radius){
        InOrOut = 3;
   }
return InOrOut;	
}

int eyeLeft(int totalrows, int totalcols, double radius, int pixRow, int pixCol){
   int InOrOut = 0;             //Flag 3 == yes, 0 == no
   double halfRad = radius/2;
   int centerx = (totalrows/2)-halfRad;
   int centery = (totalcols/2)-halfRad;

   int xoffset = totalrows*0.05;
   int yoffset = totalcols*0.05;

   if((centery-yoffset) < pixRow){
      if(pixRow < (centery+yoffset)){
         if((centerx-xoffset) < pixCol){
            if(pixCol < (centerx+xoffset)){
               InOrOut = 3;
            }
         }
      }
   }
   return InOrOut;
}

int eyeRight(int totalrows, int totalcols, double radius, int pixRow, int pixCol){
   int InOrOut = 0;             //Flag 3 == yes, 0 == no
   double halfRad = radius/2;
   int centerx = (totalrows/2)+halfRad;
   int centery = (totalcols/2)-halfRad;

   int xoffset = totalrows*0.05;
   int yoffset = totalcols*0.05;

   if((centery-yoffset) < pixRow){
      if(pixRow < (centery+yoffset)){
         if((centerx-xoffset) < pixCol){
            if(pixCol < (centerx+xoffset)){
               InOrOut = 3;
            }
         }
      }
   }
   return InOrOut;
}
