/* The header file */ 

/* Function to see if the pixel value is in the circle or not */ 
int InCircle(int totalrows, int totalcols, double radius, int pixRow, int pixCols);
int InStem(int totalrows, int totalcols, double radius, int pixRow, int pixCols);
int mouth(int totalrows, int totalcols, double radius, int pixRow, int pixCols);
int eyeLeft(int totalrows, int totalcols, double radius, int pixRow, int pixCols);
int eyeRight(int totalrows, int totalcols, double radius, int pixRow, int pixCols);
