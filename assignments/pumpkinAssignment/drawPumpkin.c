#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions.h"

/* 
 *   This is a c program to draw a pixel map shaped like a pumpkin. 
 *   Usage: 
 * 
 *   drawPumpkin OutFileName numRows numCols radius
 * 
 *   Here our program takes in an outfile name, and two arguments for
 *   the rows and columns for the size of the pumpkin to draw.  
*/ 

int main(int argc,    // This is the number of things passed into this function.  
         char *argv[] // This is the array of things passed. 
        ){

    int numRows;		 /* Place holder for the number of rows    */ 
    int numCols;   		 /* Place holder for the number of columns */ 
    int imageSize; 		 /* Total number of pixels we will use     */ 
    int row, col;  		 /* row, and column counters               */
    double radius;    		 /* Size of my circle                      */
    int InOut;     		 /* Flag where (0 = out , and 1 = circle)  */  
    int InOutS;                  /* Flag where (0 = out, and 2 = inStem    */
    int InOutM; 		 /* flag where (0 = out, and 3= inMouth)   */
    int InOutEyeL;               /* flag where (0 = out, and 3= inMouth)   */
    int InOutEyeR;               /* flag where (0 = out, and 3= inMouth)   */ 
    unsigned char *outImage;     /* pixel pointer                          */ 
    unsigned char *ptr;          /* a ponter                               */ 
    //unsigned char *outputFP;   /* Output file                            */ 
    FILE *outputFP;           
    printf("=====================================\n");  
    printf("       I'm drawing a pumpkin         \n");
    printf("=====================================\n\n");

    if(argc!=5){
        printf("Usage: ./drawPumpkin OUTfileName numrow numcols radius \n");
        exit(1);
    }
    if ( (numRows = atoi(argv[2]) ) <= 0){
        printf("Error: numRows needs to be positive");
    } 
    if ( (numCols = atoi(argv[3]) ) <= 0){
        printf("Error: numCols needs to be positive");
    }
    if ( (radius = atoi(argv[4]) ) <= 0){
        printf("Error: The radius should be positive");
    }

    // ============================================
    // Set up space to draw 
    // ============================================
    imageSize = numRows*numCols*3;  
    outImage  = (unsigned char *) malloc(imageSize); // get enough space for my image. 

    /* Open a file to put the output image into */ 
    if((outputFP = fopen(argv[1], "w")) == NULL){
        perror("output open error");
        printf("Error: can not open output file\n");
        exit(1);
    } 
    
    /* Now lets create the plain pixel map! */ 
    ptr = outImage; 
    
    /*Print the base shape */	
    for(row = 0; row < numRows; row++){
        for(col = 0; col < numCols; col++){
            // Walk through each row of the image column by column. 
            // Use a function to decide if you are in the circle or not.
            InOut = InCircle(numRows,numCols,radius,row,col);
	    InOutS = InStem(numRows, numCols, radius, row, col);
            InOutM = mouth(numRows, numCols, radius, row, col);
	    InOutEyeL = eyeLeft(numRows, numCols, radius, row, col);
            InOutEyeR = eyeRight(numRows, numCols, radius, row, col);

	    if (InOut ==1){
               /* Orange Pixel */ 
               *(ptr)   = 255;
               *(ptr+1) = 128; 
               *(ptr+2) = 1;
            }
	    if (InOutS ==2){
	       /* Green Pixel */	
	       *(ptr)  =0;
    	       *(ptr+1)=128;
	       *(ptr+2)=0;
            }	
  	    if (InOutM == 3){
               /* Black Pixel */
	       *(ptr) = 0;
               *(ptr+1)=0;
	       *(ptr+2)=0; 
            }
	    if (InOutEyeL == 3){
               /* Black Pixel */
               *(ptr) = 0;
               *(ptr+1)=0;
               *(ptr+2)=0;
            }
            if (InOutEyeR == 3){
               /* Black Pixel */
               *(ptr) = 0;
               *(ptr+1)=0;
               *(ptr+2)=0;
            }
            // Advance the pointer. 
            ptr += 3; 
        }
    }
    // Put all of this information into a file with a need header. 
    ptr= outImage;

    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);

    /* Done */ 
    fclose(outputFP);

    return 0;
}
