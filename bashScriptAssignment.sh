#!/bin/bash
fileName='./*.tex'
clear 

echo The program is now scanning 
for eachfile in $fileName
do
	echo The system is scanning $eachfile
	echo The system is compiling $eachfile
		pdflatex $eachfile
	echo The system is removing files
		rm *.aux
		rm *.log
		rm *.bib
		rm *.out
	echo The word count for $eachfile is:
		wc $eachfile			
	done	

