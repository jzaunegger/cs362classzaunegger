import sys
import os
import math
from string import whitespace

""" This is my library of functions that can be called in the letter writing main. """


# ======================= #
# Define a class          #
# ======================= #

class Address:
	def __init__(self,First,Last,ADline1,ADline2):
		self.First   = First
		self.Last    = Last
		self.ADline1 = ADline1
		self.ADline2 = ADline2

	def dumpAddress(self):
		print "Address Info:"
		print self.First
		print self.Last
		print self.ADline1
		print self.ADline2

class DocumentText:
	def _init_(self,finalAddress):
		self.address=address
		
		self.header=("\\documentclass[12pt]{article}\n"
				+ "\\usepackage{geometry}\n"
				+ "\\geometry{margin={1in,1in},vmargin={2in,1in}\n"
				+ "\\begin{document}n"
				+ "\\thispagestyle{empty}\n\n" )
		self.myAddress= "123 Totally Real Street \n\n vskip.5in \n\n"
		
		self.toAddress= (self.address.First + " " +self.address.Last + "\n\n"
				+self.address.ADline1 + " " +self.address.ADline +"\n\n")
		
		self.date= "\\vskip.5in \n  \\today \n\n vskip.5in \n\n"
		
		self.greeting= "Dear " + self.address.First + " " + self.address.Last + ", \n\n \vskip.5in"
		
		self.body= ("Didn't you know that the bird is the word? Everyone knows about the bird, everyone" + 
				"knows that the bird is the word! I said a bird, bird, bird, birds the word. Let" + 
				"me tell you about the bird, everyone knows that the word is the bird" +
				"\n\n \vskip.5in \n\n \\hskip3.5in Your Friend" +
				"\\hskip3.5in GranDaddy Mac \n\n")
	def WriteLetter(self):
		LetterOutName = str(self.address.Last.lstrip(' '))+ ".tex"
		LetterFile = open(LetterOutName,"w")
		LetterFile.write(self.header)
		LetterFile.write(self.myAddress)
		LetterFile.write(self.toAddress)
		LetterFile.write(self.date)
		LetterFile.write(self.greeting)
		LetterFile.write(self.body)
		LetterFile.write(self.footer)
		LetterFile.close		
