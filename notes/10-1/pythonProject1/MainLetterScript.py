from LetterFunctions import *

""" This is a script that will mail merge a csv file to make custom letters to mythical beings.
    To Run: 
    [user@machine directory]$ python MainLetterScript.py nameAndAddresses.txt
"""
# ======================================================================== #
# Read in command line arguements                                          #
# ======================================================================== #
if len(sys.argv) !=2:
	print("To Run: python MainLetterScript.py nameAndAddresses.txt" )
else: 
	print "Using addresses from the file", str(sys.argv[1])
	dataFile=str(sys.argv[1])

# ======================================================================= #
# Open the file and read the data                                         #
# ======================================================================= #
AddressData=open(dataFile,"r")

# ======================================================================= #
# Read data from the file, line by line 						  #
# ======================================================================= #
print "Currently reading from file: " + dataFile
AddressList=[]
for line in AddressData.readlines():
	First, Last, Address1, Address2 = map(str,line.split(','))
	CurrentAddress=Address(First, Last, Address1, Address2)
	AddressList.append(CurrentAddress)
	CurrentAddress.dumpAddress()
