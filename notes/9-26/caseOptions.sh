#!/bin/bash

while [ -n "$1" ]; do 
case "$1" in 
	-a) echo "-a option was given to me";;
	-b) echo "-b option was given to me";;
	--)
	   shift #seperate the parameters to my script
 	   break 
	   ;;    #Exits the loop and is now looking for parameters. 
		
	 esac 
shift 
done

total=1

for param in $@; do
	echo "#$total:  $param"
	total=$((total + 1))
done
