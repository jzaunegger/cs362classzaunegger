#!/bin/bash

HEIGHT=15
WIDTH=30
CHOICE_HEIGHT=4
BACKTITLE="the backt
TITLE="This is the title"
MENU="You must choose"

options=( 1 "option 1"
	  2 "option 2"
	  3 "option 3")

choice=$(dialog --clear \
		--backtitle " $BACKTITLE" \
		--title "$TITLE" \
		--menu "$menu" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${options[@]}" \
		2>&1 >/dev/tty)
clear
case $choice in
	1) echo "You choose 1" ;;
	2) echo "You choose 2" ;;
	3) echo "You choose 3" ;;
esac
